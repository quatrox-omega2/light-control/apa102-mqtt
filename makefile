##
# makefile - makefile to build apa102-mqtt.
#
# Copyright (C) 2017 Flemming Richter <quatrox@member.fsf.org>
#
# This file is part of apa102-mqtt.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
##

# main compiler
CC := gcc
#LD := gcc

APA102 := apa102
APA102_MQTT := apa102mqttd

all: $(APA102_MQTT)

$(APA102_MQTT):
	@echo "Compiling C program"
	$(CC) -std=c99 $(CFLAGS) $(APA102).c $(APA102_MQTT).c -o $(APA102_MQTT) $(LDFLAGS) -lmosquitto -lcares -lugpio

clean:
	@rm -rf $(APA102_MQTT)
