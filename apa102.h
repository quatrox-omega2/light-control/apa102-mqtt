/**
 * apa102.h - APA102 LED library
 *
 * Copyright (C) 2017 Flemming Richter <quatrox@member.fsf.org>
 *
 * This file is part of apa102-mqtt.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APA102_H
# define APA102_H
# define MY_LEDS     60
# define ELEM(_v) ( sizeof(_v) / (sizeof(_v[0])) )
# define POS_ALL (255)
# include <stdbool.h>
# include <stdint.h>

struct my_colour_t {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

struct my_led_frame_t {
	uint8_t intensity; // only the 5 least significant bits matters
	struct my_colour_t rgb;
};

extern void apa102_gpio_setup(void);
extern void apa102_gpio_teardown(void);
extern void apa102_init(const struct my_led_frame_t *);
extern void apa102_apply_states(void);

extern void apa102_set_brightness(uint8_t pos, uint8_t);
extern void apa102_get_brightness(uint8_t pos, uint8_t *);
extern void apa102_set_rgb(uint8_t pos, const struct my_colour_t *);
extern void apa102_get_rgb(uint8_t pos, struct my_colour_t *);
#endif
