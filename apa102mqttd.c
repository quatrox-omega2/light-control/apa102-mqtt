/**
 * apa102mqttd.c - APA102 MQTT daemon
 *
 * Copyright (C) 2017 Flemming Richter <quatrox@member.fsf.org>
 *
 * This file is part of apa102-mqtt.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 500
#include <assert.h> // should be the first include

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mosquitto.h>

#include "apa102.h"


struct subscriptions_t {
	const char *const name;
	int msg_id;
	bool subscribed;
};

enum e_kitchen_apa102_cmd {
	E_BL_GET,
	E_BL_SET,
	E_POW_GET,
	E_POW_SET,
	E_RGB_GET,
	E_RGB_SET,
	E_SWITCH_SET,
};
static struct subscriptions_t subscriptions[] = {
	{ "frm/kitchen/light/apa102/brightness/get", 0, false },
	{ "frm/kitchen/light/apa102/brightness/set", 0, false },
	{ "frm/kitchen/light/apa102/effect/get", 0, false },
	{ "frm/kitchen/light/apa102/effect/set", 0, false },
	{ "frm/kitchen/light/apa102/rgb/get", 0, false },
	{ "frm/kitchen/light/apa102/rgb/set", 0, false },
	{ "frm/kitchen/light/apa102/switch", 0, false },
};

struct cb_data {
	bool server_not_found;
	bool server_disconnected;
} my_cb_data;

static uint8_t verbosity = 0;
static bool my_quit = false;
static bool my_loop_enabled = false;
typedef enum e_my_connect_status {
	E_MY_NOT_CONNECTED,
	E_MY_CONNECTED,
	E_MY_DISCONNECTED,
} my_connect_status_t;
static uint8_t my_connected = E_MY_NOT_CONNECTED;
static struct mosquitto *mosq = NULL;

static int protos[] = {
	MQTT_PROTOCOL_V31,
	MQTT_PROTOCOL_V311,
};
static int proto;
static bool proto_mismatch = false;

static void my_unsubscribe(void) {
	const char *const func = "mosquitto_unsubscribe()";
	for (uint8_t i = 0; i < ELEM(subscriptions); i++) {
		struct subscriptions_t *const s = &subscriptions[i];

		if (s->name == NULL) continue;
		if (!s->subscribed) continue;
		const int rc = mosquitto_unsubscribe(mosq, &s->msg_id, s->name);
		switch (rc) {
		case MOSQ_ERR_SUCCESS:
			s->subscribed = false;
		default:
			fprintf(stderr, "[ERROR] %s failed for %s with %d\n", func, s->name, rc);
			break;
		}
	}
}

static int my_mosquitto_loop(void) {
	const int timeout_ms = 0; // return immediatly
	const int max_packets = 1; // must be 1 according to the API
	return mosquitto_loop(mosq, timeout_ms, max_packets);
}

static void my_cleanup(void) {
	apa102_gpio_teardown();
	if (mosq != NULL) {
		if (my_connected == E_MY_CONNECTED) {
			if (mosquitto_want_write(mosq)) {
				my_mosquitto_loop(); // don't care if it fails
			}
			my_unsubscribe();
		}
		if (my_connected != E_MY_NOT_CONNECTED) {
			mosquitto_disconnect(mosq);
		}
		my_connected = E_MY_NOT_CONNECTED;
		mosquitto_destroy(mosq);
		mosq = NULL;
	}
	if (mosquitto_lib_cleanup() != MOSQ_ERR_SUCCESS) {
		fprintf(stderr, "[ERROR] mosquitto_lib_cleanup() failed\n");
		exit(EXIT_FAILURE);
	}
	my_loop_enabled = false;
}

static void my_sighandler(const int sig) {
	switch (sig) {
	case SIGQUIT:
		printf("Cought SIGQUIT - cleaning up and exiting\n");
		my_quit = true;
		return;
	case SIGINT:
		my_loop_enabled = false;
		my_quit = true;
		printf("Cought SIGINT - %s MQTT loop\n", my_loop_enabled ? "enabling" : "disabling");
		return;
	}
}

/**
 * This is called when the server (aka broker) sends a CONNACK
 */
static void my_mosq_on_connect_cb(struct mosquitto *const mosq, void *const obj, const int rc) {
	switch (rc) {
	case 0:
		printf("[INFO] successfully connected to the MQTT broker\n");
		return;
	case 1:
		fprintf(stderr, "[ERROR] connection with the MQTT broker refused - protocol\n");
		proto_mismatch = true;
		return;
	case 2:
		fprintf(stderr, "[ERROR] connection with the MQTT broker refused - ID rejected\n");
		my_cleanup();
		exit(EXIT_FAILURE);
	case 3:
		if (obj != NULL) {
			struct cb_data *const data = (struct cb_data *)obj;
			data->server_not_found = true;
		}
		my_connected = E_MY_DISCONNECTED;
		printf("[INFO] MQTT broker is not available at the moment\n");
		return;
	default:
		printf("[ERROR] Unknown error -  connection status %d\n", rc);
		my_cleanup();
		exit(EXIT_FAILURE);
	}
}

/**
 * This is called when the server (aka broker) has received the DISCONNECT command
 * and has disconnected from the client
 */
static void my_mosq_on_disconnect_cb(struct mosquitto *const mosq, void *const obj, const int rc) {
	if (obj != NULL) {
		struct cb_data *const data = (struct cb_data *)obj;
		data->server_disconnected = true;
	}
	my_connected = E_MY_DISCONNECTED;
	switch (rc) {
	case 0:
		printf("[INFO] The broker has received DISCONNECT and has disconnected\n");
		break;
	default:
		fprintf(stderr, "[ERROR] The broker has unexpectedly disconnected\n");
		break;
	}
}

/**
 * This is called when a publish msg has successfully been sent to the broker
 */
static void my_mosq_on_publish_cb(struct mosquitto *const mosq, void *const obj, const int msg_id) {
	printf("[DBG] published msg %d\n", msg_id);
}

static uint8_t my_atoi_n(const char *p, const int len) {
	if (len < 1) return 0;
	if (p == NULL) return 0;
	uint8_t v = 0;
	char *a = strndup(p, len);
	if (a == NULL) {
		perror("strndup()");
	} else {
		v = atoi(a);
		free(a);
	}
	return v;
}

/**
 * This is called when a msg is received from the broker
 */
static void my_mosq_on_msg_cb(struct mosquitto *const mosq, void *const obj, const struct mosquitto_message *const m) {
	// The msg will automatically be free()ed when this function returns

	char *p = (char *)m->payload;
#if 0
	printf("[MSG RECV] ID: %d, topic: \"%s\", payload: \"%p\", qos: %d, retrain: %s\n", m->mid, m->topic, m->payload, m->qos, m->retain ? "true" : "false");
	printf("[MSG PAYLOAD]");
	for (size_t i = 0; i < m->payloadlen; i++) {
		printf(" %2X", *p);
		p++;
	}
	printf("\n");

	// E_BL_GET,
	// E_BL_SET,
	// E_POW_GET,
	// E_POW_SET,
	// E_RGB_GET,
	// E_RGB_SET,
	// E_SWITCH_SET,
	p = (char *)m->payload;
#endif
	if (!strcmp(m->topic, subscriptions[E_SWITCH_SET].name)) {
		if (m->payload == NULL) return;
		if (!strcmp(m->payload, "OFF")) {
			struct my_colour_t rgb = { 0, 0, 0 };
			apa102_set_brightness(POS_ALL, 0x00);
			//apa102_set_rgb(POS_ALL, &rgb); // to force switch off
		} else if (!strcmp(m->payload, "ON")) {
			//apa102_set_brightness(POS_ALL, 0xFF);
		} else {
			return;
		}
	} else if (!strcmp(m->topic, subscriptions[E_BL_SET].name)) {
		if (m->payload == NULL) return;
		apa102_set_brightness(POS_ALL, my_atoi_n(m->payload, m->payloadlen));
	} else if (!strcmp(m->topic, subscriptions[E_RGB_SET].name)) {
		if (m->payloadlen < 5) return; // 0,0,0
		if (p == NULL) return;
		uint8_t j = 0;
		struct my_colour_t rgb = { 0, 0, 0 };
		for (size_t i = 0; i < m->payloadlen; i++) {
			if (p == NULL) break;
			if (*p == ',') {
				j++;
				if (j > 2) break;
				p++;
				continue;
			}
			if ((*p >= '0') && (*p <= '9')) {
				switch (j % 3) {
				case 0:
					rgb.red *= 10;
					rgb.red += *p - '0';
					break;
				case 1:
					rgb.green *= 10;
					rgb.green += *p - '0';
					break;
				case 2:
					rgb.blue *= 10;
					rgb.blue += *p - '0';
					break;
				} 
			}
			p++;
		}
		apa102_set_rgb(POS_ALL, &rgb);
	} else {
		return;
	}
	apa102_apply_states();
}

/**
 * This is called when the broker responds to a subscribe request
 */
static void my_mosq_on_subscribe_cb(struct mosquitto *const mosq, void *const obj, const int msg_id, const int qos_count, const int granted_qos[]) {
	printf("[INFO] subscribed to msg ID %d\n", msg_id);
	printf("\tgranted subscriptions:");
	for (int i = 0; i < qos_count; i++) {
		printf(" %8d", granted_qos[i]);
	}
	printf("\n");
}

/**
 * This is called when the broker responds to an unsubscribe request
 */
static void my_mosq_on_unsubscribe_cb(struct mosquitto *const mosq, void *const obj, const int msg_id) {
	printf("[INFO] unsubscribed to msg ID %d\n", msg_id);
}

static void my_mosq_on_log_cb(struct mosquitto *const mosq, void *const obj, const int level, const char *const msg) {
	const char *prefixes[] = {
		"[LOG_NONE   ]",
		"[LOG_ERR    ]",
		"[LOG_WARN   ]",
		"[LOG_NOTICE ]",
		"[LOG_INFO   ]",
		"[LOG_DBG    ]",
		"[LOG_SUBSCR ]",
		"[LOG_UNSUBSC]",
		"[LOG_WEBSOCK]",
		"[LOG_ALL    ]",
		"[LOG_UNKNOWN]",
	};
	enum {
		E_LOG_NONE,
		E_LOG_INFO,
		E_LOG_NOTICE,
		E_LOG_WARN,
		E_LOG_ERR,
		E_LOG_DBG,

		E_LOG_SUBSCR,
		E_LOG_UNSUBSCR,
		E_LOG_WEBSOCKS,
		E_LOG_ALL,

		E_LOG_UNKNOWN,
	} l;
	switch (level) {
	case MOSQ_LOG_NONE:
		l = E_LOG_NONE;
		break;
	case MOSQ_LOG_ERR:
		l = E_LOG_ERR;
		break;
	case MOSQ_LOG_WARNING:
		l = E_LOG_WARN;
		break;
	case MOSQ_LOG_NOTICE:
		l = E_LOG_NOTICE;
		break;
	case MOSQ_LOG_INFO:
		l = E_LOG_INFO;
		break;
	case MOSQ_LOG_DEBUG:
		l = E_LOG_DBG;
		break;
	case MOSQ_LOG_SUBSCRIBE:
		l = E_LOG_SUBSCR;
		break;
	case MOSQ_LOG_UNSUBSCRIBE:
		l = E_LOG_UNSUBSCR;
		break;
	case MOSQ_LOG_WEBSOCKETS:
		l = E_LOG_WEBSOCKS;
		break;
	case MOSQ_LOG_ALL:
		l = E_LOG_ALL;
		break;
	default:
		l = E_LOG_UNKNOWN;
		break;
	}
	switch (level) {
	case MOSQ_LOG_WARNING:
	case MOSQ_LOG_ERR:
	case MOSQ_LOG_NOTICE:
	case MOSQ_LOG_INFO:
		printf("%s %s\n", prefixes[l], msg);
		break;
	}
}

static void my_setup(void) {
	const int signals[] = { SIGINT, SIGQUIT };
	for (uint8_t i = 0; i < ELEM(signals); i++) {
		if (signal(signals[i], my_sighandler) == SIG_ERR) {
			perror("signal()");
			exit(EXIT_FAILURE);
		}
	}	
	if (mosquitto_lib_init() != MOSQ_ERR_SUCCESS) {
		fprintf(stderr, "[ERROR] mosquitto_lib_init() failed\n");
		exit(EXIT_FAILURE);
	}
	my_cb_data.server_not_found = false;
	my_cb_data.server_disconnected = false;
	mosq = mosquitto_new("light_kitchen_apa102", true, &my_cb_data);
	if (mosq == NULL) {
		perror("mosquitto_new()");
		my_cleanup();
		exit(EXIT_FAILURE);
	}
	//mosquitto_will_clear()
	//mosquitto_will_set()
	//mosquitto_username_pw_set()

	{
		int o = proto;
		mosquitto_opts_set(mosq, MOSQ_OPT_PROTOCOL_VERSION, &o);
	}
	mosquitto_connect_callback_set(mosq, my_mosq_on_connect_cb);
	mosquitto_disconnect_callback_set(mosq, my_mosq_on_disconnect_cb);
	mosquitto_publish_callback_set(mosq, my_mosq_on_publish_cb);
	mosquitto_message_callback_set(mosq, my_mosq_on_msg_cb);
	mosquitto_subscribe_callback_set(mosq, my_mosq_on_subscribe_cb);
	mosquitto_unsubscribe_callback_set(mosq, my_mosq_on_unsubscribe_cb);
	mosquitto_log_callback_set(mosq, my_mosq_on_log_cb);
	//mosquitto_reconnect_delay_set() - only if using mosquitto_loop_forever()
	mosquitto_max_inflight_messages_set(mosq, 8); // the default is 20, which is rather high
	mosquitto_message_retry_set(mosq, 4); // # of sec before retry - default is 20
	

	{
		const char *const func = "mosquitto_connect()";
		const char *const hostname = "mqtt-server.lan";
		const uint16_t port = 1883;
		const uint8_t ping_interval = 60;
		const int rc = mosquitto_connect(mosq, hostname, port, ping_interval);
		switch (rc) {
		case MOSQ_ERR_SUCCESS:
			my_connected = E_MY_CONNECTED;
			break;
		case MOSQ_ERR_INVAL:
			fprintf(stderr, "[ERROR] %s: invalid params\n", func);
			break;
		case MOSQ_ERR_ERRNO:
			perror("mosquitto_connect()");
			break;
		default: // We should never get here
			fprintf(stderr, "[ERROR] %s returned %d\n", func, rc);
			break;
		}
		if (rc != MOSQ_ERR_SUCCESS) {
			my_cleanup();
			exit(EXIT_FAILURE);
		}
	}
	apa102_gpio_setup();
}

static void my_process_mqtt_topics(void) {
	while ((my_loop_enabled) && (!my_quit) && (!proto_mismatch) && (my_connected == E_MY_CONNECTED)) {
		const char *const func = "mosquitto_loop()";
		const int rc = my_mosquitto_loop();
		switch (rc) {
		case MOSQ_ERR_SUCCESS:
			break;
		case MOSQ_ERR_INVAL:
			fprintf(stderr, "[ERROR] %s - EINVAL\n", func);
			my_cleanup();
			exit(EXIT_FAILURE);
		case MOSQ_ERR_NOMEM:
			fprintf(stderr, "[ERROR] %s - ENOMEM\n", func);
			my_cleanup();
			exit(EXIT_FAILURE);
		case MOSQ_ERR_NO_CONN:
		case MOSQ_ERR_CONN_LOST:
			my_connected = E_MY_DISCONNECTED;
			fprintf(stderr, "[WARN] connection lost\n");
			return; // this will trigger a restart
		case MOSQ_ERR_PROTOCOL:
			fprintf(stderr, "[ERROR] protocol error detected\n");
			proto_mismatch = true;
			return; // try reconnecting
		case MOSQ_ERR_ERRNO:
			perror("mosquitto_loop()");
			return; // try reconnecting
		default: // We should never get here
			fprintf(stderr, "[ERROR] %s returned %d\n", func, rc);
			my_cleanup();
			exit(EXIT_FAILURE);
		}

		//mosquitto_message_copy()
		usleep(5000);
	}
}

static void my_subscribe(void) {
	for (uint8_t i = 0; i < ELEM(subscriptions); i++) {
		const char *const func = "mosquitto_subscribe()";
		struct subscriptions_t *const s = &subscriptions[i];

		if (s->name == NULL) continue;
		printf("[DBG] subscribing to MQTT service \"%s\"\n", s->name);
		const int qos = 2; // deliver the msg exactly once
		const int rc = mosquitto_subscribe(mosq, &s->msg_id, s->name, qos);
		switch (rc) {
		case MOSQ_ERR_SUCCESS:
			s->subscribed = true;
			break;
		case MOSQ_ERR_INVAL:
			fprintf(stderr, "[ERROR] %s: failed to subscribe to %s - EINVAL\n", func, s->name);
			break;
		case MOSQ_ERR_NOMEM:
			fprintf(stderr, "[ERROR] %s: failed to subscribe to %s - ENOMEM\n", func, s->name);
			break;
		case MOSQ_ERR_NO_CONN:
			my_connected = E_MY_DISCONNECTED;
			fprintf(stderr, "[ERROR] %s: failed to subscribe to %s - client is not connected\n", func, s->name);
			break;
		default: // We should never get here
			fprintf(stderr, "[ERROR] %s: failed to subscribe to %s - %d\n", func, s->name, rc);
			break;
		}
		if (rc != MOSQ_ERR_SUCCESS) {
			my_cleanup();
			exit(EXIT_FAILURE);
		}
	}
}

int main(const unsigned int argc, const char *const argv[], const char *const envp[]) {
	if (argc > 1) {
		const int v = atoi(argv[1]);
		const char *const mosq_err = mosquitto_strerror(v);
		const char *const mosq_conn_status = mosquitto_connack_string(v);
		if (mosq_err != NULL) printf("mosquitto error code %d is \"%s\"\n", v, mosq_err);
		if (mosq_conn_status != NULL) printf("mosquitto connection status %d is \"%s\"\n", v, mosq_conn_status);
		return 0;
	}
	{
		const struct my_led_frame_t f = {
			.intensity = 0x01,
			.rgb.red = 0xff,
			.rgb.green = 0xff,
			.rgb.blue = 0xff,
		};
		apa102_gpio_setup();
		apa102_init(&f);
	}
	printf("[INFO] Starting %s. To look up error codes, call %s <error_code>\n\n", argv[0], argv[0]);
	proto = protos[0];
	while (!my_quit) {
		assert(my_connected != E_MY_CONNECTED);
		my_loop_enabled = true;
		if (proto_mismatch) {
			proto = (proto + 1) % ELEM(protos);
			proto_mismatch = false;
		}
		printf("[DBG] setting up MQTT client\n");
		my_setup();
		my_subscribe();
		//mosquitto_publish()

		my_process_mqtt_topics();
	
		my_cleanup();
		usleep(500 * 1000);
	}

	return 0;
}

