/**
 * apa102.c - APA102 LED library
 *
 * Copyright (C) 2017 Flemming Richter <quatrox@member.fsf.org>
 *
 * This file is part of apa102-mqtt.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE 500

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <ugpio/ugpio.h>
#include "apa102.h"


#define MY_GPIO_LO false
#define MY_GPIO_HI true

static uint8_t verbosity = 0;
static struct my_led_frame_t my_states[MY_LEDS];
static struct my_led_frame_t my_state;

enum e_my_gpios {
	E_MY_CLK_PIN = 0,
	E_MY_DATA_PIN = 1,
};

static const int gpios[] = { E_MY_CLK_PIN, E_MY_DATA_PIN };

static uint8_t my_atoi(const char *const str) {
	if (str == NULL) {
		return -1;
	}	
	errno = 0;

	{
		char *endptr;
		const int base = 0;
		const long val = strtol(str, &endptr, base);
		
		if ((errno == ERANGE && (val == LONG_MAX || val == LONG_MIN)) || (errno != 0 && val == 0)) {
			perror("strtol()");
			exit(EXIT_FAILURE);
		}
		if (endptr == str) {
			fprintf(stderr, "[ERROR] Invalid number \"%s\"\n", str);
			exit(EXIT_FAILURE);
		}
		if (*endptr != '\0') {
			fprintf(stderr, "[WARNING] Ignoring \"%s\" in \"%s\"\n", endptr, str);
		}
		if (val < 0) {
			fprintf(stderr, "[ERROR] \"%s\" should not be negative\n", str);
			exit(EXIT_FAILURE);
		}
		if (val > 0xff) {
			fprintf(stderr, "[ERROR] \"%s\" should not exceed 255\n", str);
			exit(EXIT_FAILURE);
		}
		return (uint8_t)val;
	}
}

static void my_clk(const bool hi) {
	const bool is_inverted = true;
	const int v = ((hi && (!is_inverted)) || ((!hi) && is_inverted)) ? 1 : 0;
	gpio_set_value(gpios[E_MY_CLK_PIN], v);
	usleep(100);
}

static void my_data(const bool hi) {
	const bool is_inverted = true;
	const int v = ((hi && (!is_inverted)) || ((!hi) && is_inverted)) ? 1 : 0;
	gpio_set_value(gpios[E_MY_DATA_PIN], v);
	usleep(100);
}

static void write_bits(const bool bits[], const uint8_t len) {
	for (uint8_t i = 0; i < len; i++) {
		my_clk(MY_GPIO_LO);
		my_data(bits[i]);
		my_clk(MY_GPIO_HI);
	}
}

static void write_byte(const uint8_t byte) {
	for (int8_t bit = 7; bit > -1; bit--) {
		const bool v = ((byte >> bit) & 0x01) ? MY_GPIO_HI : MY_GPIO_LO;
		my_clk(MY_GPIO_LO);
		my_data(v);
		my_clk(MY_GPIO_HI);
	}
}

static void write_bytes(const uint8_t bytes[], const uint8_t len) {
	for (uint8_t i = 0; i < len; i++) {
		for (int8_t bit = 7; bit > -1; bit--) {
			const bool v = ((bytes[i] >> bit) & 0x01) ? MY_GPIO_HI : MY_GPIO_LO;
			my_clk(MY_GPIO_LO);
			my_data(v);
			my_clk(MY_GPIO_HI);
		}
	}
}

static void send_start_frame(void) {
	if (verbosity > 2) printf("[INFO] sending start frame\n");
	const uint8_t bytes[] = { 0x00, 0x00, 0x00, 0x00 };
	write_bytes(bytes, ELEM(bytes));
}

static void send_led_frame(const struct my_led_frame_t *const f) {
	if (verbosity > 2) printf("[INFO] sending LED frame  %2X  %2X %2X %2X\n", f->intensity, f->rgb.blue, f->rgb.green, f->rgb.red);
	write_byte(f->intensity);
	write_byte(f->rgb.blue);
	write_byte(f->rgb.green);
	write_byte(f->rgb.red);
}

static void send_end_frame(void) {
	if (verbosity > 2) printf("[INFO] sending end frame\n");
	const uint8_t bytes[] = { 0xFF, 0xFF, 0xFF, 0xFF };
	write_bytes(bytes, ELEM(bytes));
}



static bool gpios_setup(const bool setup) {
	static int rq[ELEM(gpios)];
	int rv[ELEM(gpios)];
	static bool ok = true;
	static bool has_been_setup = false;

	if (setup && !has_been_setup) {
		// check if GPIOs are already exported
		for (uint8_t i = 0; i < ELEM(gpios); i++) {
			rq[i] = gpio_is_requested(gpios[i]);
			if (rq[i] < 0) {
				perror("gpio_is_requested()");
				exit(EXIT_FAILURE);
			}
		}

		// export GPIOs
		for (uint8_t i = 0; i < ELEM(gpios); i++) {
			if (rq[i] > 0) continue;
			rv[i] = gpio_request(gpios[i], NULL);
			if (rv[i] < 0) {
				perror("gpio_request()");
				for (uint8_t j = 0; j < i; j++) {
					if (rq[j] > 0) continue;
					if (gpio_free(gpios[j]) < 0) {
						perror("gpio_free()");
					}
				}
				exit(EXIT_FAILURE);
			}
		}

		for (uint8_t i = 0; i < ELEM(gpios); i++) {
			if (gpio_direction_output(i, 0) < 0) {
				perror("gpio_direction_output()");
				ok = false;
				break;
			}
		}
		has_been_setup = true;
	} else if (has_been_setup && !setup) {
		for (uint8_t i = 0; i < ELEM(gpios); i++) {
			if (rq[i] > 0) continue;
			if (gpio_free(gpios[i]) < 0) {
				perror("gpio_free()");
			}
		}
		has_been_setup = false;
	}
	if (setup) return ok;
	return true;
}

void apa102_apply_states(void) {
	if (verbosity > 0) printf("[DBG] entering %s\n", __func__);
	if (gpios_setup(true)) {
		send_start_frame();
		for (uint8_t i = 0; i < ELEM(my_states); i++) {
			send_led_frame(&my_states[i]);
		}
		send_end_frame();
	}
}

void apa102_gpio_setup(void) {
	if (verbosity > 0) printf("[DBG] entering %s\n", __func__);
	gpios_setup(true);
}

void apa102_gpio_teardown(void) {
	if (verbosity > 0) printf("[DBG] entering %s\n", __func__);
	gpios_setup(false);
}

void apa102_init(const struct my_led_frame_t *const v) {
	if (v == NULL) return;
	if (verbosity > 0) printf("[DBG] entering %s( { %u, { %u, %u, %u } } )\n", __func__, v->intensity, v->rgb.red, v->rgb.green, v->rgb.blue);
	struct my_led_frame_t state = *v;
	state.intensity |= 0x80 | 0x40 | 0x20;
	for (uint8_t i = 0; i < ELEM(my_states); i++) {
		my_states[i] = state;
	}
	my_state = state;
}

void apa102_set_brightness(const uint8_t pos, uint8_t v) {
	if (verbosity > 0) printf("[DBG] entering %s(%u, %u)\n", __func__, pos, v);
	v |= 0x80 | 0x40 | 0x20;
	if (pos == POS_ALL) {
		for (uint8_t i = 0; i < ELEM(my_states); i++) {
			my_states[i].intensity = v;
		}
	} else if (pos < ELEM(my_states)) {
		my_states[pos].intensity = v;
	} else {
		return;
	}
	my_state.intensity = v;
}

void apa102_get_brightness(const uint8_t pos, uint8_t *const v) {
	if (verbosity > 0) printf("[DBG] entering %s(%u, %u)\n", __func__, pos, *v);
	if (v == NULL) return;
	if (pos < ELEM(my_states)) {
		*v = my_states[pos].intensity;
	} else if (pos == POS_ALL) {
		*v = my_state.intensity;
	} else {
		*v = 0;
	}
}

void apa102_set_rgb(const uint8_t pos, const struct my_colour_t *const v) {
	if (verbosity > 0) printf("[DBG] entering %s(%u, { %u, %u, %u })\n", __func__, pos, v->red, v->green, v->blue);
	if (v == NULL) return;
	if (pos == POS_ALL) {
		for (uint8_t i = 0; i < ELEM(my_states); i++) {
			my_states[i].rgb = *v;
		}
	} else if (pos < ELEM(my_states)) {
		my_states[pos].rgb = *v;
	} else {
		return;
	}
	my_state.rgb = *v;
}

void apa102_get_rgb(const uint8_t pos, struct my_colour_t *const v) {
	if (verbosity > 0) printf("[DBG] entering %s(%u, { %u, %u, %u })\n", __func__, pos, v->red, v->green, v->blue);
	if (v == NULL) return;
	if (pos < ELEM(my_states)) {
		*v = my_states[pos].rgb;
	} else if (pos == POS_ALL) {
		*v = my_state.rgb;
	} else {
		v->red = 0;
		v->green = 0;
		v->blue = 0;
	}
}

