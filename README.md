<a name="apa102-mqtt" \>
---
<p align="center">
	<a href="https://gitlab.com/quatrox-omega2/light-control/apa102-mqtt/blob/master/LICENSE">
		<img src="https://img.shields.io/badge/license-GPLv3%2B-blue.svg" alt="GPLv3+" />
	</a>
	<a href="https://gitlab.com/quatrox-omega2/light-control/apa102-mqtt/blob/master/README.md">
	</a>
</p>
---
<p align="center"><b>
apa102-mqtt enables controlling APA102 LEDs via MQTT on the Onion Omega2 platform.
</b></p>

### Build instructions
TODO

### Installation
TODO

### Features

- can control one APA102 LED strip
- embedded MQTT client
- runs as daemon

